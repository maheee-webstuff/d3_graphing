import { Observable, of } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

export class DataService {
  private static _instance: DataService;

  static Instance(): DataService {
      return this._instance || (this._instance = new this());
  }

  private data$: Observable<any>;

  getData(): Observable<any> {
    if (!this.data$) {
      this.data$ = of(this.getDataInt()).pipe(
        shareReplay(1)
      );
    }
    return this.data$;
  }

  private getDataInt() {
    return {
      "resources": [
        {
          "id": "00000000-0000-0000-0000-000000000000",
          "displayName": "root",
          "members": [
            {
              "type": "GROUP",
              "value": "10000000-1000-0000-0000-000000000000"
            },
            {
              "type": "GROUP",
              "value": "10000000-2000-0000-0000-000000000000"
            }
          ]
        },
        {
          "id": "10000000-1000-0000-0000-000000000000",
          "displayName": "level 1-1",
          "members": [
            {
              "type": "GROUP",
              "value": "20000000-1000-0000-0000-000000000000"
            },
            {
              "type": "GROUP",
              "value": "20000000-2000-0000-0000-000000000000"
            }
          ]
        },
        {
          "id": "10000000-2000-0000-0000-000000000000",
          "displayName": "level 1-2",
          "members": [
          ]
        },
        {
          "id": "20000000-1000-0000-0000-000000000000",
          "displayName": "level 2-1",
          "members": [
          ]
        },
        {
          "id": "20000000-2000-0000-0000-000000000000",
          "displayName": "level 2-2",
          "members": [
          ]
        },
      ]
    };
  }
}
