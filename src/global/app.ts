
window['mbar__open'] = () => {
  if (!document.getElementsByTagName('mbar-container').length) {
    const element = document.createElement('mbar-container');
    element.classList.add('closed');
    document.getElementsByTagName('body')[0].appendChild(element);
  }

  const event = new CustomEvent('mbar__event', { detail: { action: 'open' } });
  window.dispatchEvent(event);
}

window['mbar__close'] = () => {
  const event = new CustomEvent('mbar__event', { detail: { action: 'close' } });
  window.dispatchEvent(event);
}
