import { Component, Element, Prop, h } from '@stencil/core';
import * as d3 from 'd3';

@Component({
  tag: 'd3-graph',
  styleUrl: 'd3-graph.scss',
  shadow: true
})
export class D3Graph {

  @Element() private element: HTMLElement;

  @Prop() data: any = null;

  handleChange(event) {
    console.log(event);
  }

  componentDidLoad() {
    const width = 460;
    const height = 460;

    while (this.element.shadowRoot.firstChild) {
      this.element.shadowRoot.removeChild(this.element.shadowRoot.firstChild);
    }

    const d3root = d3.select(this.element.shadowRoot);
    const tree = d3.tree().size([height, width - 100]);
    const root = d3.hierarchy(this.data.resources[0], currentNode =>
      this.data.resources.filter(node => 
        currentNode.members.map(member => member.value).indexOf(node.id) > -1
      )
    );
    tree(root);


    const svg = d3root
      .append("svg")
        .attr("width", width)
        .attr("height", height)
      .append("g")
        .attr("transform", "translate(40,0)");  // bit of margin on the left = 40

    // Add the links between nodes:
    svg.selectAll('path')
      .data( root.descendants().slice(1) )
      .enter()
      .append('path')
      .attr("d", (node) => {
        const n = node as any;
        const p = node.parent as any;
        return "M" + n.y + "," + n.x
                + "C" + (p.y + 50) + "," + n.x
                + " " + (p.y + 150) + "," + p.x // 50 and 150 are coordinates of inflexion, play with it to change links shape
                + " " + p.y + "," + p.x;
      })
      .style("fill", 'none')
      .attr("stroke", '#ccc')


    // Add a circle for each node.
    svg.selectAll("g")
      .data(root.descendants())
      .enter()
      .append("g")
      .attr("transform", (node) => {
        const n = node as any;
        return "translate(" + n.y + "," + n.x + ")"
      })
      .append("circle")
        .attr("r", 7)
        .style("fill", "#69b3a2")
        .attr("stroke", "black")
        .style("stroke-width", 2)
      .append('svg:title')
        .text((d, i) => (d.data as any).displayName);
  }

  render() {
    return (
      <div>
        <h1>Graph loading ...</h1>
      </div>
    );
  }

}
