import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

export const config: Config = {
  namespace: 'd3_graphing',
  srcDir: 'src',
  globalStyle: 'src/global/app.css',
  globalScript: 'src/global/app.ts',
  outputTargets: [
    {
      type: 'www',
      baseUrl: 'https://maheee-webstuff.gitlab.io/d3_graphing/'
    },
    {
      type: 'dist'
    }
  ],
  plugins: [
    sass()
  ]
};
