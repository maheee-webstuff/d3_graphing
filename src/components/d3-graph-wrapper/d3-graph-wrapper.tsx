import { Component, State, h } from '@stencil/core';
import { Subscription } from 'rxjs';
import { DataService } from '../../services/data.service';

@Component({
  tag: 'd3-graph-wrapper',
  styleUrl: 'd3-graph-wrapper.scss',
  shadow: true
})
export class D3GraphWrapper {

  @State() private data: any = null;
  @State() private error: any = null;

  private dataSubscription: Subscription;

  componentDidLoad() {
    const dataService = DataService.Instance();
    this.dataSubscription = dataService.getData().subscribe({
      next: data => { this.data = data; this.error = null; },
      error: error => { this.data = null; this.error = error; }
    });
  }

  componentDidUnload() {
    this.dataSubscription.unsubscribe();
  }

  render() {
    if (!this.data && !this.error) {
      return <div>Loading ...</div>
    } else if (this.data) {
      return <d3-graph data={this.data}></d3-graph>;
    } else {
      return <div>Error!</div>
    }
  }

}
